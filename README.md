# ElviraSchedule

## Problem Statement

Assign (not necessarily all) shifts to employees for a four week period such that:

- All employees have two consecutive free days per week
- All employees work one full weekend plus one day another weekend (and are free rest of weekends)
- A shift has at most one employee any given day
- Employees gets as close to their guaranteed number of hours for the four week period as possible
- There is an unassigned shift the first and third sunday
- There is an employee who can be in charge weekdays 11.30-20.30 and weekends 11-18
- As many days as possible have someone working from 11.30 that is over 20 years old
- Each employee works at most six days in a row

## Objective

Undecided, possibly just maximize number of hours for the employees as it seems
they do not get enough.

## Instance Data

### Four employees:

| #    | Hours / week | Can Be In Charge | Over 20 |
| ---- | ------------ | ---------------- | ------- |
| 1    | 40          | Yes              | No      |
| 2    | 40         | Yes              | Yes     |
| 3    | 40         | Yes              | Yes     |
| 4    | 30         | No               | Yes     |

### Weekday Shifts:

8-14, 10-17, 11.30-20.30, 17-20.30

### Weekend Shifts:

9-16, 11-18, 12-18
